#!/bin/bash

SNMP_CONF_PATH="/etc/snmp/snmpd.conf"
DOWNLOADED_MODULES_DIR="plethora-master"
DOWNLOADED_MODULES_ZIP="master.zip"
MODULES_DIR=$PLETHORA_CONF"modules"
PLETHORA_MODULES_URL="https://github.com/CDeschietere/plethora/archive/master.zip"
CONFIG_FILE="plethora.conf"

#######################################################################################################
#
#	Check if the script is lunched with root rights
#
#######################################################################################################


if [ "$EUID" -ne 0 ]; then 
	echo "Please run as root"
  	exit 1
fi



#######################################################################################################
#
#	Check if PLETHORA_CONF is set
#
#######################################################################################################

if [ -z "$PLETHORA_CONF" ]; then
	echo "Set PLETHORA_CONF (the path where the agent will be installed) in /etc/environment"
	exit 1
fi




#######################################################################################################
#
#	Install snmp if it's not istalled
#
#######################################################################################################

if ! dpkg-query -l snmp > /dev/null; then
	apt-get install snmp
fi

if ! dpkg-query -l snmpd > /dev/null; then
	apt-get install snmpd
fi



#######################################################################################################
#
#	Modify the SNMPD's configuration file for make all OIDs available by localhost
#
#######################################################################################################

sed -i 's/^[#].*rocommunity.*public.*localhost/ rocommunity public localhost/g' $SNMP_CONF_PATH

sed -i 's/^[^#].*rocommunity[^0-9].*public.*default.*-V.*systemonly/#&/g' $SNMP_CONF_PATH

sed -i 's/^[^#].*rocommunity[0-9].*public.*default.*-V.*systemonly/#&/g' $SNMP_CONF_PATH



#######################################################################################################
#
#	Check create folder needed by the agent by using PLETHORA_CONF
#
#######################################################################################################

if [ -d $MODULES_DIR ]; then
	rm -r $MODULES_DIR
fi

echo "$MODULES_DIR"

if [ ! -d $MODULES_DIR ]; then
  mkdir -p $MODULES_DIR;
fi

#######################################################################################################
#
#	Download and install JARS for the agent
#
#######################################################################################################

wget $PLETHORA_MODULES_URL

unzip $DOWNLOADED_MODULES_ZIP


cp $CONFIG_FILE $PLETHORA_CONF

for module in $DOWNLOADED_MODULES_DIR/*.jar; do

	while [ -z "$response" ]; do
		echo "Do you want to install this module : ${module##*/} [Y/N]"
		read response
	done

	if [ "${response,,}" == "y" ]; then
		cp $module $MODULES_DIR
	fi
	response=""

done

rm $DOWNLOADED_MODULES_ZIP

rm -r $DOWNLOADED_MODULES_DIR


